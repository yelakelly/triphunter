let path = require("path");
let webpack = require('webpack');
let BundleTracker = require('webpack-bundle-tracker');


module.exports = {
  context: __dirname,
  devtool: 'source-map',

  entry: [
    './frontend/js/main'
  ],
  
  output: {
      path: path.resolve('./frontend/bundles/'),
      filename: "[name]-[hash].js",
  },

  plugins: [
    new webpack.NoEmitOnErrorsPlugin(), 
    new BundleTracker({filename: './webpack-stats.json'}),
  ],

  resolve: {
    extensions: ['*', '.js', '.css', '.scss'],
    alias: {
      leafletCss: path.join(__dirname, '/node_modules/leaflet/dist/leaflet.css'),
      handlebars: path.join(__dirname, '/node_modules/handlebars/dist/handlebars.min.js'),
      leafletMeasure: path.join(__dirname, '/node_modules/leaflet-measure/dist/leaflet-measure.ru.js')
    }
  },

  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
          }
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ],
      },
      { 
        test: /\.styl$/, 
        loader: 'style-loader!css-loader!stylus-loader' 
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          publicPath: 'static/bundles/',
          outputPath: 'img/'
        }
      }
    ],
  },
}
