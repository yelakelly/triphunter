# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-05-08 08:08
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('triphunter', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('coord', django.contrib.gis.db.models.fields.PolygonField(srid=4326)),
                ('name', models.CharField(blank=True, default='', max_length=200)),
            ],
        ),
    ]
