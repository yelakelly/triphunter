from django.contrib.gis.admin import OSMGeoAdmin
from django.contrib import admin

from .models import Sight, Region, SightCategories, Meta


class SightAdmin(OSMGeoAdmin):
    search_fields = ['title']

admin.site.register(Sight, SightAdmin)
admin.site.register(Region)
admin.site.register(SightCategories)
admin.site.register(Meta)