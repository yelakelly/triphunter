from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.routers import DefaultRouter

from triphunter import views

router = DefaultRouter()
router.register(r'sights', views.SightViewSet)
router.register(r'regions', views.RegionViewSet)
router.register(r'users', views.UserViewSet)
router.register(r'sights_categories', views.SightCategoriesViewSet)

urlpatterns = [
    url(r'$^', views.render_index),
    url(r'^', include(router.urls)),
    url(r'region_list', views.RegionList.as_view()),
    url(r'get_nearest_sights', views.NearestSightsList.as_view()),
    url(r'get_popular_places', views.get_popular_places),
    url(r'heat_map', views.HeatMap.as_view()),
    url(r'make_sight_route', views.make_sight_route),
]