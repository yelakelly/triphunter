from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from .models import Sight, Region, SightCategories, SightImages
from base.rest_utils import ObjectMethodField
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    sights = serializers.PrimaryKeyRelatedField(many=True, queryset=Sight.objects.all())

    class Meta:
        model = User
        fields = ('id', 'username', 'sights')

class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = ('id', 'name')

class SightImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = SightImages
        fields = ('id','image')

class SightCategoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = SightCategories
        fields = ('id', 'name')

class SightSerializer(GeoFeatureModelSerializer):
    description = ObjectMethodField(method='get_small_description')

    class Meta:
        geo_field = 'geom'
        model = Sight
        
        fields = ('id', 'title', 'geom', 'description')

class SightSerializerFull(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    images_set = SightImageSerializer(many=True, read_only=True)
    pagination_class = None

    class Meta:
        model = Sight
        fields = ('id', 'title', 'description', 'address', 'owner', 'city', 'images_set')

class RegionSerializerList(serializers.ModelSerializer):
    pagination_class = None
    class Meta:
        model = Region
        fields = ('id', 'name')

class MetaSerializer(serializers.Serializer):
    key = serializers.CharField()
    value = serializers.IntegerField()

class HeatMapSerializer(MetaSerializer):
    content_object = SightSerializer()