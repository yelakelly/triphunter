import json
import requests

from django.contrib.auth.models import User
from django_filters.rest_framework import DjangoFilterBackend
from django.contrib.gis.geos import Point, Polygon, LineString
from django.core.serializers import serialize
from django.template.response import TemplateResponse
from django.db import connection
from django.db.models import Count

from rest_framework import permissions, viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.pagination import PageNumberPagination
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework import filters
from rest_framework.pagination import LimitOffsetPagination

from .utils import WikiParser, with_metric_buffer, find_nearest_points
from .models import Sight, Region, SightCategories, Meta
from .permissions import IsOwnerOrReadOnly
from .serializers import SightSerializer, UserSerializer, RegionSerializer, SightCategoriesSerializer, HeatMapSerializer, RegionSerializerList, SightSerializerFull

GET_AREAS_BY_POPULARITY = '''
    SELECT
        CONCAT(n.name,' ', n.rtype),
        SUM(m.value::bigint) as meta
    FROM triphunter_sight AS s
        JOIN triphunter_region AS n
            ON st_within(s.geom, n.coord)
        JOIN triphunter_meta as m
            ON (s.id = m.object_id)
        WHERE m.key = 'media_count'
    GROUP BY n.id ORDER by meta DESC LIMIT 10;
'''

GET_CITIES_BY_POPULARITY = '''
    SELECT SUM(triphunter_meta.value::bigint) as c, triphunter_sight.city FROM triphunter_meta
        LEFT JOIN triphunter_sight ON (triphunter_meta.object_id = triphunter_sight.id) WHERE key='media_count'
    GROUP BY  triphunter_sight.city ORDER BY  c DESC LIMIT 10;
'''

GET_AREAS_BY_SIGHT_COUNT = '''
    SELECT
        CONCAT(n.name,' ', n.rtype),
        count(s.id) AS count_of_city
    FROM triphunter_sight AS s
        JOIN triphunter_region AS n
            ON st_within(s.geom, n.coord)
    GROUP BY n.id ORDER by count_of_city DESC LIMIT 10;
'''

GET_NEAREST_SIGHTS_BY_DISTANCE = '''
  SELECT * FROM (SELECT id, title, distance FROM (SELECT
    id,
    title,
    ST_Distance(
        ST_Transform(ST_SetSRID(ST_Point(39.69922542572022, 47.23833590938506),4326),26986),
        ST_Transform(ST_SetSRID(ST_POINT(ST_X(geom), ST_Y(geom)), 4326),26986)
    ) as distance
    FROM (SELECT id, title, geom
          FROM triphunter_sight
          WHERE ST_Distance_Sphere(geom, ST_MakePoint(39.715662002563484, 47.23518897862626)) <= 1 * 1609.34
         )
  AS t ORDER BY distance) as foo GROUP BY foo.id, foo.title, foo.distance) as b WHERE distance < %(limit)s;
'''

def render_index(request):
    return TemplateResponse(request, 'index.html')

@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'users': reverse('users-list', request=request, format=format),
        'sights': reverse('sights-list', request=request, format=format)
    })

@api_view(['POST'])
@permission_classes((AllowAny,))
def make_sight_route(request, format=None):
    route_type = request.data.get('params').get('type')
    coords = request.data.get('params')['location']
    route_time = request.data.get('params')['route_time']

    max_distance = int(route_time) * 83.3333

    first_point = Point(coords[0], coords[1])
    circle = with_metric_buffer(first_point, float(max_distance * 2) / 2)
    sights = Sight.objects.filter(geom__within=circle).values('geom')
    points = find_nearest_points(first_point, list(sights), [first_point], 0, max_distance)

    query_points = [str(coord.x) + ',' + str(coord.y) for coord in points]

    if(len(query_points)):
        OSRM_URL = 'https://osrmserver.xyz/route/v1/driving/{0}?overview=false&steps=true'.format(';'.join(query_points))

        response = requests.get(OSRM_URL)
        route = json.loads(response.content)['routes'][0]
        total_distance = route['distance']

        if total_distance > max_distance:
            new_distance = 0
            legs = route['legs']
            for idx, leg in enumerate(legs):
                distance = leg['distance']
                if new_distance + distance > max_distance:
                    break
                new_distance += distance

            points = points[0:idx]

        return Response([[coord.y, coord.x] for coord in points])
    else:
        return Response([])

@api_view(['GET'])
@permission_classes((AllowAny,))
def get_popular_places(request, format=None):
    area = request.GET.get('area')
    chart_type = request.GET.get('type')
    sights = []

    if area == 'sights':
        obj = Meta.objects.filter(key='media_count').extra({'int_value': "CAST(value as BIGINT)"}).order_by('-int_value').values_list('object_id', 'value')[0:10]
        sights = Sight.objects.filter(pk__in=[o[0] for o in obj]).values('title')
        for idx, sight in enumerate(sights):
            sight['value'] = obj[idx][1]
        sights = list(sights)

    if area == 'city' and chart_type:
        if chart_type == 'count':
            sights = Sight.objects.values('city').annotate(Count('city')).order_by('-city__count')[0:10]
            sights = list(sights)
        elif chart_type == 'popularity':
            with connection.cursor() as cursor:
                cursor.execute(GET_CITIES_BY_POPULARITY)
                sights = cursor.fetchall()

    if area == 'area' and chart_type:
        if chart_type == 'count':
            query = GET_AREAS_BY_SIGHT_COUNT
        elif chart_type == 'popularity':
            query = GET_AREAS_BY_POPULARITY
        
            with connection.cursor() as cursor:
                cursor.execute(query)
                sights = cursor.fetchall()

    return Response(sights)

class SightCategoriesViewSet(viewsets.ModelViewSet):
    queryset = SightCategories.objects.all()
    serializer_class = SightCategoriesSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly,)
    pagination_class = PageNumberPagination

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class SightViewSet(viewsets.ModelViewSet):
    queryset = Sight.objects.all()
    serializer_class = SightSerializer

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    pagination_class = PageNumberPagination

    filter_backends = (filters.SearchFilter,)
    search_fields = ('^title',)

    def get_queryset(self, *args, **kwargs):
        queryset = self.queryset
        filters = self.request.GET.get('filters')

        if(filters):
            filters = json.loads(filters)

            if len(filters.keys()):
                queryset = Sight.objects
                for key, val in filters.items():
                    if key == 'regions':
                        queryset = queryset.filter(region__in=val)
                    if key == 'types':
                        queryset = queryset.filter(types__name__in=val)
            
        return queryset

    
    def get_serializer_class(self, *args, **kwargs):
        if self.request.GET.get('full') != None:
            return SightSerializerFull
        else:
            return self.serializer_class

class RegionViewSet(viewsets.ModelViewSet):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    pagination_class = PageNumberPagination
    search_fields = ('name')

    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('id',)

class NearestSightsList(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        current_coord = request.data.get('position')
        radius = request.data.get('radius')
        
        _point = Point(current_coord[1], current_coord[0])
        circle = with_metric_buffer(_point, float(radius) / 2)
        sights = Sight.objects.filter(geom__within=circle)

        ids = [sight.id for sight in sights]
            
        return Response({
            'circle': circle.geojson,
            'points': ids
        })

class RegionList(APIView):
   permission_classes = (AllowAny,)
   def get(self, request, format=None):
        res = RegionSerializerList(Region.objects.order_by('name').defer("coord"), many=True)
        return Response(res.data)

class HeatMap(APIView):
   permission_classes = (AllowAny,)

   def get(self, request, format=None):
        bbox = request.GET.get('bbox')
        poly = Polygon.from_bbox(list(map(float, bbox.split(','))))
        sights = Sight.objects.filter(geom__within=poly)
        meta_instances = Meta.objects.filter(key='media_count', object_id__in=[x['id'] for x in list(sights.values('id'))])

        data = HeatMapSerializer(meta_instances, many=True).data
        return Response({
            "max_media_count": Meta.get_max_media_count(meta_instances),
            "instances": data
        })

class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer