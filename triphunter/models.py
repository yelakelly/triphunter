from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import Max
from django.contrib.gis.db.models import PointField, MultiPolygonField
from django.contrib.gis import geos
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType


from .utils import _image_upload

class Region(models.Model):
    coord = MultiPolygonField(null=False, blank=False)
    name = models.CharField(max_length=200, blank=True, default='')
    rtype = models.CharField(max_length=500, blank=True, default='')

    class Meta:
        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'

    def __str__(self):
        return self.name

class SightCategories(models.Model):
    name = models.CharField(max_length=400, blank=False)
    keywords = ArrayField(models.CharField(max_length=400), blank=True)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
    
    def __str__(self):
        return self.name


class Sight(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=200, blank=True, default='')
    description = models.TextField(max_length=20000)
    address = models.CharField(max_length=400, blank=True, default='')
    types = models.ManyToManyField(SightCategories)
    geom = PointField(null=True, blank=True,)
    owner = models.ForeignKey('auth.User', related_name='sights', on_delete=models.CASCADE)
    region = models.ForeignKey(Region, on_delete=None, related_name='region_inst', null=True, blank=True)
    city = models.CharField(max_length=2000, blank=True)

    def get_small_description(self):
        return self.description.splitlines()[0]

    def save(self, *args, **kwargs):
        geom = self.geom if self.geom is not None else None
        if geom is not None and self.region is None:
            _region = Region.objects.filter(coord__contains=geom)
            if len(_region):
                self.region = _region[0]
        super(Sight, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Достопримечательность'
        verbose_name_plural = 'Достопримечательности'
        ordering = ('created',)


class SightImages(models.Model):
    sight = models.ForeignKey(Sight, related_name='images_set', verbose_name='Объект')
    image = models.ImageField(upload_to=_image_upload, verbose_name='Изображение')
    
    class Meta:
        verbose_name = 'Изображение объекта'
        verbose_name_plural = 'Изображения объекта'

    def __str__(self):
        return 'Изображение для {}'.format(str(self.sight))


class Meta(models.Model):
    key = models.CharField(max_length=4000, null=True, blank=True, verbose_name="Ключ")
    value = models.CharField(max_length=4000, null=True, blank=True, verbose_name="Значение")
    last_updated = models.DateTimeField(auto_now=True)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return 'Мета для объекта {0} типа {1}'.format(self.object_id, self.content_type.name)

    class Meta:
        verbose_name = 'Метаданные'
        verbose_name_plural = 'Метаданные'

    @staticmethod
    def get_max_media_count(qs):
        if(qs):
            qs = qs.extra({'int_value': "CAST(value as BIGINT)"}).order_by('-int_value')[0]
        else:
            qs = Meta.objects.filter(key='media_count').extra({'int_value': "CAST(value as BIGINT)"}).order_by('-int_value')[0]
        return qs.int_value
