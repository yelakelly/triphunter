from bs4 import BeautifulSoup
from urllib.request import urlopen
from multiprocessing import Process

import json

import sqlite3
from sqlite3 import Error
 
CONNECTION = None

sql_create_sights = """CREATE TABLE IF NOT EXISTS sights (
 id integer PRIMARY KEY,
 name text NOT NULL,
 description text,
 images text,
 coords text
);"""

def create_connection(db_file):
    """ create a database connection to a SQLite database """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

def create_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


ROOT_URL = 'https://localway.ru'

page = urlopen(ROOT_URL)
soup = BeautifulSoup(page, 'html.parser')

def parse(city, conn):
    href = city.attrs.get('href')

    if(href):
        town_url = '{0}{1}section/sights'.format(ROOT_URL, href)
        town_page = urlopen(town_url)
        town_soup = BeautifulSoup(town_page, 'html.parser')
        next_links = town_soup.find_all('a', attrs={'class': 'lw-pagination__page'})
        
        if(next_links):
            last = next_links[-1].text

            for inner in range(1, int(last) + 1):
                inner_url = town_url + '/page/' + str(inner)
                inner_soup = BeautifulSoup(urlopen(inner_url), 'html.parser')
                inner_links = find_inner_links(inner_soup)
                
                get_content(inner_links, conn)
        else:
            inner_links = find_inner_links(town_soup)
            get_content(inner_links, conn)

def get_content(links, conn):
    for sight_link in links:
        sight_url = ROOT_URL + sight_link
        sight_soup = BeautifulSoup(urlopen(sight_url), 'html.parser')

        data = sight_soup.find('script', attrs={'id': 'lw-page-data'}).text
        data = json.loads(data)

        images = data.get('poi').get('images')
        coords = data.get('poi').get('coords')
        name = data.get('poi').get('name')
        description = data.get('poi').get('description')

        data = {
            'images': json.dumps(images),
            'coords': json.dumps(coords),
            'name': name,
            'description': description
        }
        
        if(not name or not description or not coords):
            continue

        print(data['name'], data['coords'])

        create_sight(conn, data)

def find_inner_links(_soup):
    links = []

    for link in _soup.find_all('a', attrs={'class': 'lw-recommendation__poi'}):
        shref = link.attrs.get('href')
        links.append(shref)

    return links

cities = soup.find_all('a', attrs={'class': 'lw-city-selector__item'})

def _func1():
    conn = create_connection("temp1.db")
    create_table(conn, sql_create_sights)

    for item in cities[0:43]:
        parse(item, conn)

def _func2():
    conn = create_connection("temp2.db")
    create_table(conn, sql_create_sights)

    for item in cities[43:86]:
        parse(item, conn)

def _func3():
    conn = create_connection("temp3.db")
    create_table(conn, sql_create_sights)

    for item in cities[86:-1]:
        parse(item, conn)

def create_sight(conn, data):
    sql = ''' INSERT INTO sights(name, description, images, coords)
              VALUES(:name,:description,:images,:coords) '''
    cur = conn.cursor()
    cur.execute(sql, data)
    conn.commit()
    return cur.lastrowid

from functools import partial

if __name__ == '__main__':
    try:
        conn = sqlite3.connect('db_primary.sqlite3')
    except Error as e:
        print(e)
    p1 = Process(target=_func1)
    p1.start()
    
    p2 = Process(target=_func2)
    p2.start()
    
    p3 = Process(target=_func3)
    p3.start()
    
    p1.join()
    p2.join()
    p3.join()