import wikipedia
import re
wikipedia.set_lang("ru")

class WikiParser():
    def clear_images(self, images):
        ALLOW_FORMATES = ['jpg', 'png', 'jpeg']
        clean_images = []

        for img in images:
            ext = img.split('.')[-1]
            if(ext in ALLOW_FORMATES):
                clean_images.append(img)

        return clean_images

    def find_keywords(self):
        for index, cat in enumerate(self.categories):
            self.categories[index] = re.sub(r'^(\w+:)+', '', self.categories[index])
            self.categories[index] = re.sub(r'[0-9]', '', self.categories[index])

        cats = ' '.join(str(x).lower() for x in self.categories).split(' ')
        keywords = []

        for keyword in cats:
            if(len(keyword) > 4 and not keyword in keywords):
                keywords.append(keyword)
        
        return keywords

    def __init__(self, page):
        page = wikipedia.page(title=page)
        self.content = page.content
        self.images = self.clear_images(page.images)
        self.categories = page.categories
        self.links = page.links
        
print(WikiParser('Церковь_Сурб_Хач_(Ростов-на-Дону)').find_keywords())

# print(page.categories)
# print(page.content)
# print(page.coordinates)
# print(page.images)

# Сохранить контент и убрать лишнее
# Привести в соответствие категории
# Сохранить местоположение
# Скачать изображения

# words = [
#     'достопримечательность',
#     'музей',
#     'река',
#     'крепость',
#     'монастырь',
#     'скульптурное произведение',
#     'скульптура',
#     'объект культурного наследия россии федерального значения'
#     'церковь',
# ]

# s = wikipedia.geosearch(latitude=47.290417, longitude=39.721944, radius=1000)
# print(s)