from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.gis.gdal import OGRGeometry, DataSource
from django.contrib.gis import geos

import json

from triphunter.models import Region

class Command(BaseCommand):

    def handle(self, *args, **options):
        ds = DataSource('geo.json')
        layer = ds[0]

        for feat in layer:
            poly = feat.geom
            print(poly.geom_name)
            if(feat.geom.geom_name == 'POLYGON'):
                poly = geos.MultiPolygon(geos.fromstr(str(feat.geom)))
            o = Region(name=feat.get('NAME_1'), rtype=feat.get('TYPE_1'), coord=poly.wkt)
            o.save()