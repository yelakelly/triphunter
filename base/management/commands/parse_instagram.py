from fuzzywuzzy import fuzz
import instagram
import time

from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.gis.gdal import OGRGeometry, DataSource
from django.contrib.gis import geos
from django.contrib.gis.geos import Point

import os.path
import json

from triphunter.models import Sight, Meta

sql_select_nearest_points = '''
    SELECT id FROM (SELECT id, title
        FROM triphunter_sight
        WHERE ST_Distance_Sphere(geom, ST_MakePoint({0})) <= 1 * 1609.34) as t WHERE title = '{1}';
'''

def sort_by_similaraty(a):
    fuzz_a = fuzz.ratio(a['name'].lower(), a['sight'].title.lower())
    return fuzz_a

class Command(BaseCommand):
    def handle(self, *args, **options):
        
        if(not os.path.isfile("data.txt")):
            f = open("data.txt","w+", encoding="utf-8")
        else:
            f = open("data.txt","a+", encoding="utf-8")
        
        agent = instagram.AgentAccount("red__neck_", "091295v3v33", "sqrt361@yandex.ru")
        agent.update()
        sights = Sight.objects.filter(id__gt=2189)

        for sight in sights:
            time.sleep(2)
            title = sight.title
            
            search = instagram.Search(sight.title)
            results = agent.update(search)

            places = results.get('places')
            
            if places:
                _places = []

                for place in places:
                    lat = place['place']['location'].get('lat')
                    lng = place['place']['location'].get('lng')
                    name = place['place']['location'].get('name')
                    _id = place['place']['location']['pk']
                    facebook_id = place['place']['location'].get('facebook_places_id')
                    
                    if(lat and lng):
                        loc = str(lng) + ',' + str(lat)
                        
                        nearest = Sight.objects.raw(sql_select_nearest_points.format(loc, title))
                        nearest = list(nearest)

                        if(len(nearest)):
                            data = {}
                            
                            if(_id):
                                data['instagram_id'] = _id
                            if(facebook_id):
                                data['facebook_id'] = facebook_id

                            _places.append({
                                'name': name,
                                'sight': nearest[0],
                                'data': data
                            })
                
                places_count = len(_places)

                if(places_count):
                    if(places_count > 1):
                        _places = sorted(_places, key=sort_by_similaraty, reverse=True)
                        sight_place = _places[0]['data']
                    else:
                        sight_place = _places[0]['data']

                    
                    sight_id = _places[0]['sight'].id
                    sight_title = _places[0]['sight'].title
                    place_name = _places[0]['name']

                    print(sight_id, sight_title, place_name, sight_place)

                    f.write('{0},{1},{2},{3}\n'.format(
                        sight_id,
                        sight_title,
                        place_name,
                        sight_place
                    ))
                    
                    if(sight_place.get('instagram_id')):
                        Meta(content_object=sight, key='instagram_id', value=sight_place['instagram_id']).save()

                    if(sight_place.get('facebook_id')):
                        Meta(content_object=sight, key='facebook_id', value=sight_place['facebook_id']).save()
        f.close()
            # count += 1
            # if(count == 200):
            #     break

            # search = instagram.Search("Гроты Большого каскада")

            # results = agent.update(search)
            # print(results)
