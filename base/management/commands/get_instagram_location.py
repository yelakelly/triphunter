import instagram
import requests
import time

from django.db.models import Q
from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand

import json

from triphunter.models import Sight, Meta

LAST_LOCATION_ID = 2294


def parse_locations(LAST_LOCATION_ID):
    
    try:
        time.sleep(3)
        instagram_instances = Meta.objects.filter(key='instagram_id', object_id__gt=LAST_LOCATION_ID)
        agent = instagram.Agent()
        sight_type = ContentType.objects.get_for_model(Sight).id

        for instance in instagram_instances:
            location = instagram.Location(instance.value)
            agent.update(location)

            sight = Sight.objects.get(pk=instance.object_id)
            media_count = location.media_count
            
            count = Meta.objects.filter(content_type__pk=sight_type, object_id=sight.id, key='media_count')

            if(len(count)):
                count[0].value = media_count
            else:
                Meta(content_object=sight, key='media_count', value=media_count).save()

            print(sight.id)
            LAST_LOCATION_ID = sight.id
            
    except instagram.InternetException as e:
        time.sleep(30)
        print(e.error)
        print('FAILED')
        parse_locations(LAST_LOCATION_ID)

class Command(BaseCommand):
    def handle(self, *args, **options):
        parse_locations(LAST_LOCATION_ID)
