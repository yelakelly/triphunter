import requests
import time

from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand
from django.contrib.contenttypes.models import ContentType

import json

from triphunter.models import Sight, Meta

class Command(BaseCommand):
    def handle(self, *args, **options):
        url = "https://geocode-maps.yandex.ru/1.x/"
        sights = Sight.objects.filter(city='')
        sight_type = ContentType.objects.get_for_model(Sight).id

        for obj in sights:
            time.sleep(1)
            coords = obj.geom.coords

            data = {
                'geocode': str(coords[0]) + ',' + str(coords[1]),
                'format': 'json',
                'kind': 'locality'
            }

            response = requests.get(url, params=data)
            response = json.loads(response.text)['response']

            print(response)
            components = (response
            .get("GeoObjectCollection")
            .get("featureMember")[0]
            .get("GeoObject")
            .get("metaDataProperty")
            .get("GeocoderMetaData")
            .get("Address")
            .get("Components"))
            
            city = [obj for obj in components if obj['kind'] == 'locality']
            city = city[0]['name']

            #has_meta = Meta.objects.filter(content_type__pk=sight_type, object_id=obj.id, key='geocoding')
            #geo = text

            # if(has_meta):
            #     has_meta[0].value = geo
            #     has_meta[0].save()
            # else:
            #     Meta(content_object=obj, key='geocoding', value=geo).save()

            if(city):
                obj.city = city
                obj.save()
            else:
                print('Not found:', obj.title)