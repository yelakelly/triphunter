from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.contrib.gis.geos import Point
from django.core.files.base import ContentFile

import sqlite3
import requests
import json

from urllib.parse import urlparse
from PIL import Image
from sqlite3 import Error
from io import BytesIO

from triphunter.models import Sight, SightCategories, SightImages

def get_sights(conn):
    sql = '''SELECT id, name, description, images, coords FROM sights_cop WHERE id > 4018'''
    cur = conn.cursor()
    data = cur.execute(sql)
    return data

def get_byte_instance(url):
    data = requests.get(url).content
    return BytesIO(data)

class Command(BaseCommand):
    def handle(self, *args, **options):
        user = User.objects.first()
        conn = sqlite3.connect('C:\Works\Projects\djangovue\db_primary')

        for row in get_sights(conn):
            _id, name, description, images, coords = row
            images = json.loads(images)
            coords = json.loads(coords)

            try:
                Sight.objects.get(title=name)
                print(name)
                continue
            except:
                pass

            print(_id)

            sight = Sight(
                title=name, 
                geom=Point(float(coords['lng']), float(coords['lat'])),
                description=description,
                owner=user
            )

            keywords = [w.lower() for w in name.split(' ')]
            cats = SightCategories.objects.filter(keywords__overlap=keywords)

            if(cats):
                sight.categories = cats

            sight.save()

            for image in images:
                image_url = image['fullsize']

                image_instance = SightImages()
                image_instance.sight = sight

                content = ContentFile(get_byte_instance(image_url).getvalue())
                name = urlparse(image_url).path.split('/')[-1]

                image_instance.image.save(name, content, save=True)
                image_instance.save()