from rest_framework import serializers

class ObjectMethodField(serializers.Field):
    """Вызывает указанный метод у объекта и возвращает результат."""

    def __init__(self, method=None, prefix="", **kwargs):
        kwargs.pop('read_only', None)
        kwargs['source'] = '*'
        self.method_name = method
        self.prefix = prefix
        super(ObjectMethodField, self).__init__(read_only=True, **kwargs)

    def to_representation(self, obj):
        method_name = self.prefix + (self.method_name or self.field_name)
        return getattr(obj, method_name)()