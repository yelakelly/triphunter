import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import VueCarousel from 'vue-carousel';
import VueResource from 'vue-resource'
import app_css from '../stylus/app.styl'

let VueCookie = require('vue-cookie');

[
  VueResource,
  Vuetify,
  VueCookie,
  VueCarousel
].forEach((_module) => Vue.use(_module))

Vue.http.headers.common['X-CSRFToken'] = Vue.cookie.get('csrftoken');

window.App = new Vue({
  el: '#app',
  render: h => h(App),
})
