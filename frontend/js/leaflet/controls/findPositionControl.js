// if the module has no dependencies, the above pattern can be simplified to
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define([], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.findPosition = factory();
  }
}(typeof self !== 'undefined' ? self : this, function() {
    let currentPositionMarker;

    function makeTemplate(){
        let template = document.createElement('a');
        template.classList.add('map-findposition-control-button', 'btn', 'info');
        template.innerHTML = '<i aria-hidden="true" class="icon material-icons">location_searching</i>';
        return template;
    }

    function setLocation(map, icon){
        navigator.geolocation.getCurrentPosition((pos) => {
            let {latitude: lat, longitude: lon} = pos.coords;
            map.panTo([lat, lon]);

            if (currentPositionMarker) {
                currentPositionMarker.remove();   
            }

            currentPositionMarker = L.marker([lat, lon], {
                'icon': icon
            }).addTo(map)
            .bindPopup('Ваше местоположение.')
            .openPopup();

        }, (err) => console.warn(`ERROR(${err.code}): ${err.message}`), {timeout:10000});
    }

    return function(map, icon){
        let template = makeTemplate();

        template.addEventListener('click', () => {
            setLocation(map, icon);
        });

        window.onload = function(){
            setLocation(map, icon);
        };

        L.Control.findPosition = L.Control.extend({
            onAdd: function(map) {
                return template;
            }
        });

        L.control.findposition = function(opts) {
            return new L.Control.findPosition(opts);
        }

        return L.control.findposition;
    }
}));