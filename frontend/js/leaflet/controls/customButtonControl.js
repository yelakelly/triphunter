// if the module has no dependencies, the above pattern can be simplified to
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define([], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.findPosition = factory();
  }
}(typeof self !== 'undefined' ? self : this, function() {

    function makeTemplate(iconName) {
        let template = document.createElement('a');
        template.classList.add('map-custom-button-control', 'btn', 'info');
        template.innerHTML = `<i aria-hidden="true" class="icon material-icons">${iconName}</i>`;
        return template;
    }

    return function(settings){
        L.Control.CustomButtonControl = L.Control.extend({
            onAdd: function(map) {
                return template;
            }
        });

        L.control.customButtonControl = function(opts) {
            return new L.Control.CustomButtonControl(opts);
        }

        let {map, icon, remove, click} = settings;

        let template = makeTemplate(icon);

        template.addEventListener('click', () => {
            remove();
        });

        return L.control.customButtonControl;
    }

}));