function getByCity(type, data){
    let digits, labels, mainLabel;

    if(type == 'count'){
      mainLabel = 'Города по количеству объектов'
      labels = data.map((obj) => obj.city)
      digits = data.map((obj) => obj['city__count']);
    } else {
      mainLabel = 'Популярность по данным из соцсетей'
      labels = data.map((obj) => obj[1])
      digits = data.map((obj) => obj[0])
    }

    return {digits, labels, mainLabel}
  }

  function getBySights(type, data){
    let digits, labels, mainLabel;
    return {digits, labels, mainLabel};
  }

  function getByArea(type, data){
    let digits, labels, mainLabel;

    if(type == 'count'){
        mainLabel = 'Регионы по количеству объектов'
        labels = data.map((obj) => obj[0])
        digits = data.map((obj) => obj[1])
    } else{
      mainLabel = 'Популярность по данным из соцсетей'
      labels = data.map((obj) => obj[0])
      digits = data.map((obj) => obj[1])
    }

    return {digits, labels, mainLabel};
  }

  function getPopularSights(data){
    let digits, labels, mainLabel;

    mainLabel = 'Самые популярные достопримечательности';
    labels = data.map((obj) => obj['title']);
    digits = data.map((obj) => obj['value']);

    return {digits, labels, mainLabel};
  }

  export {getByCity, getBySights, getByArea, getPopularSights}