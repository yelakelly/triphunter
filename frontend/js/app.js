import ThForms from './components/Forms.vue'

import L from 'leaflet'

import 'leaflet.markercluster/dist/MarkerCluster.css'
import 'leaflet.markercluster/dist/MarkerCluster.Default.css'
import 'leaflet.markercluster/dist/leaflet.markercluster'
import 'leaflet-routing-machine/dist/leaflet-routing-machine.css'
import 'leaflet-contextmenu/dist/leaflet.contextmenu.css'
import 'leaflet-measure/dist/leaflet-measure.css'

import 'leaflet-routing-machine';
import 'leaflet-contextmenu';

import { HeatLayer, heatLayer } from "leaflet";
import "leaflet.heat";

const Measure = require('leafletMeasure');

const findPositionControl = require('./leaflet/controls/findPositionControl');
const customButtonControl = require('./leaflet/controls/customButtonControl');

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

const ICONS = {
    'default': L.icon({
        iconUrl: '/static/img/icons/placeholder-7.svg',
        iconSize: [40, 40],
    }),
    'location': L.icon({
        iconUrl: '/static/img/icons/pin.svg',
        iconSize: [40, 40],
    }),
    'nearest': L.icon({
        iconUrl: '/static/img/icons/pin-33.svg',
        iconSize: [40, 40],
    }),
};

const routeContextMixin = function(){
    return [
        {
            text: 'Добавить точку маршрута',
            icon: '/static/img/icons/placeholder-1.svg',
            callback: (e) => {
                let coord = [e.latlng.lng, e.latlng.lat];

                if(e.relatedTarget) {
                    this.wayPointsIcon = false;
                    coord = e.relatedTarget.feature.geometry.coordinates;
                } else{
                    this.wayPointsIcon = true;
                }
            
                this.wayPoints.push(L.latLng(coord[1], coord[0]));
            } 
        }, {
            text: 'Очистить маршрут',
            icon: '/static/img/icons/route.svg',
            callback: () => (this.wayPoints = [])
        }
    ];
};

let updateFilterValue = function(n, o, filterKey, dataKey){
    if (n.length > o.length) {
        if(!this.sightFilters[filterKey]){
            this.sightFilters[filterKey] = [];
        }

        let key = n[n.length - 1];
        this.sightFilters[filterKey].push(key[dataKey]);
    } else if(o.length && !n.length){
        delete this.sightFilters[filterKey];
    } else{
        let key = o[o.length - 1];
        this.sightFilters[filterKey] = this.sightFilters[filterKey].filter((item) => item !== key[dataKey]);
    }
};

export default {
    data: () => ({
        activeLayers: [],
        drawer: true,
        snackbar: false,
        dialogs: {
            'nearest': false,
            'routes': false,
            'stats': false,
            'sight_detail': false
        },
        map: null,
        theme: true,
        y: 'top',
        x: 'right',
        mode: '',
        timeout: 6000,
        nearestPoints: [],
        types: [],
        mapRouteControl: null,
        typesLength: 5,
        wayPoints: [],
        wayPointsIcon: true,
        regionsLength: 5,
        settings: [
            'Добавить объект',
            'Выбрать Регион'
        ],
        snackBarText: '',
        sightFilters: {},
        search: '',
        sightDetailData: null,
        regions: [],
        sightsResultSet: [],
        searchQuery: null,
        searchQ: null,
        selectedRegions: [],
        selectedTypes: [],
        loading:false
    }),
    components: {
        ThForms
    },
    computed: {
        filteredTypes(){
            return this.search ? this.types.filter(type => {
                return type.name.toLowerCase().includes(this.search.toLowerCase())
            }) : [];
        },
        filteredRegions(){
            return this.search ? this.regions.filter(region => {
                return region.name.toLowerCase().includes(this.search.toLowerCase())
            }) : [];
        }
    },
    props: {
        source: String
    },
    mounted() {
        this.init();
    },
    watch: {
        searchQ(val) {
            this.$http.get('/sights/', {
                'params': {
                    'search': val,
                    'limit': 5
                }
            }).then((response) => {
                if(!response.body.count) return;
                
                let result = [];
                let data = response.body.results.features;

                data.forEach((sight) => {
                    result.push({
                        'title': sight.properties.title,
                        'geom': sight.geometry.coordinates,
                    });
                });

                this.sightsResultSet = result;
            });
        },
        wayPoints(){
            if (this.mapRouteControl !== null) {
                this.map.removeControl(this.mapRouteControl);
            }

            if (this.wayPoints.length) {
                let settings = {
                    waypoints: this.wayPoints,
                    language: 'ru',
                    serviceUrl: 'https://osrmserver.xyz/route/v1'
                };

                if(!this.wayPointsIcon){
                    settings['createMarker'] = () => null;
                }

                this.mapRouteControl = L.Routing.control(settings).addTo(this.map);
            }
        },
        selectedRegions(n, o){
            updateFilterValue.call(this, n, o, 'regions', 'id');
        },
        selectedTypes(n, o){
            updateFilterValue.call(this, n, o, 'types', 'name');
        }
    },
    methods: {
        init(){
            this.initMap();
            this.$http.get('/region_list').then(function(response) {
                this.regions = response.body;
            });
        },
        // createMarker(e){
        //     console.log(e);
        // },
        getMap(){
            return this.map;
        },
        searchNearestPoints(radius){
            let map = this.map;

            if (this.nearestRemoveButton) {
                this.nearestRemoveButton.remove();
            }

            navigator.geolocation.getCurrentPosition((pos) => {
                let {latitude: lat, longitude: lon} = pos.coords;

                this.$http.post('/get_nearest_sights', {
                    'position': [lat, lon],
                    'radius': radius
                }).then(function(response) {
                    let points = response.body.points;

                    if(this.searchCircle){
                        this.searchCircle.remove();
                        this.searchCircle = null;
                    }

                    this.sightsLayer.eachLayer((layer) => {
                        let layerID = layer.feature.id;
                        points.forEach((id) => {
                            if (id == layerID) {
                                layer.setIcon(ICONS.nearest);
                                this.nearestPoints.push(layer);
                            } 
                        });
                    });
                    
                    this.searchCircle = L.geoJSON(JSON.parse(response.body.circle)).addTo(map);

                    this.nearestRemoveButton = customButtonControl({
                        'icon': 'near_me',
                        map,
                        'remove': () => {
                        this.clearNearestPoints();
                        }
                    })({ position: 'bottomleft' }).addTo(this.map);
                });
            });
        },
        searchPoint(){
            if(this.searchQuery){
                let lng = this.searchQuery[0];
                let lat = this.searchQuery[1];

                this.map.setView({
                    'lon': lng,
                    'lat': lat
                }, 19);

                this.sightsLayer.eachLayer(function(layer){
                    let layerLatLng = layer.getLatLng();

                    if(layerLatLng.lat == lat && layerLatLng.lng == lng){
                        layer.openPopup();
                    }
                });
            }
        },
        clearNearestPoints(){
            this.nearestPoints.forEach((point) => {
                point.setIcon(ICONS.default);
            });

            this.searchCircle.remove();
            this.nearestRemoveButton.remove();
        },
        createHeatMap() {
            if (this.heatMap) {
                this.heatLayerButton.remove();
                this.heatMap.remove();
            }

            this.$http.get('/heat_map', {
                'params': {
                    'bbox': this.map.getBounds().toBBoxString()
                }
            }).then((response) => {
                let geoms = [];
                let data = response.body;
                
                data.instances.forEach(feature => {
                    let geom = feature.content_object.geometry.coordinates;
                    let mediaCount = feature.value;

                    [geom[0], geom[1]] = [geom[1], geom[0]]
                    
                    geom.push(mediaCount)
                    geoms.push(geom);
                });
                
                this.heatMap = L.heatLayer(geoms, {max: data.max_media_count, radius: 25}).addTo(this.map);
                this.heatLayerButton.addTo(this.map);
            });
        },
        makeMapFilters(){
            if(this.markersCluster){
                this.map.removeLayer(this.markersCluster);
            }

            this.createSightLayer({
                'filters': JSON.stringify(this.sightFilters),
            });
        },
        createControls(){
            findPositionControl(this.map, ICONS.location)({ position: 'topright' }).addTo(this.map);
            
            let measureControl = new L.Control.Measure({ position: 'topright',
                primaryLengthUnit: 'kilometers', 
                secondaryLengthUnit: 'meters'
            });

            measureControl.addTo(this.map);

            this.heatLayerButton = customButtonControl({
                'icon': 'layers',
                map,
                'remove': () => {
                    this.heatLayerButton.remove();
                    this.heatMap.remove();
                }
            })({ position: 'bottomleft' });
        },
        createSightLayer(params){
            this.$http.get('/sights', {
                params: params
            }).then((response) => {
                let self = this;
                let markersCluster = L.markerClusterGroup();

                this.sightsLayer = L.geoJSON(response.body.results, {
                    onEachFeature: function(feature, layer){
                        layer.setIcon(ICONS.default);
                        layer.bindTooltip(feature.properties.title);

                        layer.bindPopup(`
                            <p>${feature.properties.description}</p>
                            <button class="leaflet-button" @id="${feature.id}" onclick="App.$children[0].showDetail(${feature.id})">Подробнее</button>
                        `);

                        layer.bindContextMenu({
                            contextmenu: true,
                            contextmenuItems: routeContextMixin.call(self).concat([{
                            text: 'Удалить точку маршрута',
                            icon: '/static/img/icons/placeholder-3.svg',
                            callback: (e) => {
                                let coord = e.relatedTarget.feature.geometry.coordinates;

                                self.wayPoints.forEach((point, index) => {
                                    if(point.lat == coord[1] && point.lng == coord[0]){
                                        self.wayPoints.splice(index, 1);
                                    }
                                });
                            } 
                            }]),
                            contextmenuInheritItems: false,
                        });
                    }
                });

                this.markersCluster = markersCluster.addLayer(this.sightsLayer);
                this.map.addLayer(markersCluster);
            });
        },
        initMap(){
            let self = this;

            let menuItems = [{
                text: 'Показать координаты',
                icon: '/static/img/icons/compass-2.svg',
                callback: (e) => {
                    this.snackBarText = `${e.latlng.lat}, ${e.latlng.lng}`;
                    this.snackbar = true;
                }
                }, {
                    text: 'Центрировать карту',
                    icon: '/static/img/icons/map-20.svg',
                    callback: (e) => this.map.panTo(e.latlng)
                }, {
                    text: 'Построить тепловую карту',
                    icon: '/static/img/icons/map-28.svg',
                    callback: (e) => this.createHeatMap()
            }].concat(routeContextMixin.call(this));

            navigator.geolocation.getCurrentPosition((pos) => {
                let {latitude: lat, longitude: lon} = pos.coords;

                let map = L.map('map', {
                    attributionControl: false,
                    contextmenu: true,
                    contextmenuItems: menuItems,
                }).setView([lat, lon], 12);

                //let map = L.map('map').setView([lat, lon], 12);

                this.map = map;
                this.createControls();
                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

                this.createSightLayer();
            });

            this.$http.get('/sights_categories').then((response) => {
                this.types = response.body.results;
            });
        },
        makePath(routePointType, routeTime){
            this.wayPointsIcon = false;

            let createLinePath = (pos) => {
                this.$http.post('make_sight_route', {
                        'params': {
                            'type':  routePointType,
                            'route_time': routeTime,
                            'location': pos
                        }
                    }).then((response) => {
                      if(response.body.length > 1){
                          this.wayPoints = response.body;
                          this.routeLayer = L.geoJSON(response.body);
                          this.map.addLayer(this.routeLayer);
                      } else {
                          this.snackBarText = 'Ближайшие точки не найдены';
                          this.snackbar = true;
                      }
                });
            };

            if(routePointType == 'point'){
                this.map.once('click', (e) => {
                    createLinePath([e.latlng.lng, e.latlng.lat]);
                    this.map.off('click', event);
                });
            } else {
                navigator.geolocation.getCurrentPosition((pos) => {
                    createLinePath([pos.coords.longitude, pos.coords.latitude]);
                });
            }

        },
        showDetail(id){
            this.$http.get(`/sights/${id}?full`).then((response) => {
                this.sightDetailData = response.body;
                this.dialogs.sight_detail = true;
            });
        }
    }
}